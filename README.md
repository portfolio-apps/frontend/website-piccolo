# Piccolo - React Contact Form Designed with Figma

### Netlify
https://dazzling-jackson-ffd6ec.netlify.app/  
### Figma Mock-up
https://www.figma.com/file/kyia5mvsPGh1k3YhLBFmbY/Untitled?node-id=0%3A1

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.
